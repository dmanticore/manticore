import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';


class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      username:'',
      password:'',
      dialogTitle: '',
    };
  }
  render() {
    const hostname = window.location.host;
    const textStyle = ''
    return (
      <div>
      <table id="wrapper" class="login">
      <tbody>
        <tr>
          <td class="side">
            <div class="header">
              <div class="">
                <div class="line_1">
                  <div class="logo_1"><img src={'//'+hostname+'/logo.svg'} /></div>
                  <div class="logo_2">
                    <span class="green">ML</span>PLATFORM
                  </div>
                  <div class="sub_title">Start your ML journey here...</div>
                  <span class="clear"></span>
                </div>
              </div>
            </div>
            <div class="form_welcome">
              <div class="">
                <div class="forms">
                  <form id="login-form2" action="/site/login" method="post">
                    <div class="landing">
                      <div class="btns">
                        <div>
                            <div>
                              <input placeholder="Username" onChange={this.usernameChange} />
                              <br/>
                              <input placeholder="Password" onChange={this.passwordChange} />
                              <br/>
                              <button onClick={this.handleClick.bind(this)}>Login</button>
                            </div>
                          <Dialog open={!!this.state.dialogTitle} keepMounted={true} onClose={() => this.setState({ dialogTitle: ''})}>
                            <DialogTitle>
                                {this.state.dialogTitle}
                            </DialogTitle>
                            <DialogActions>
                              <Button onClick={() => this.setState({ dialogTitle: ''})} color="primary">
                                Close
                              </Button>
                            </DialogActions>
                          </Dialog>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </td>
          <td class="content">		
            <video autoPlay muted loop id="myVideo">
              <source src={'//'+hostname+'/loop.mp4'} type="video/mp4"/>
            </video>
            <div class="message">
              <span class="msg1">EXPLORE</span> <span class="msg2"> &#8226;  DEVELOP </span><span class="msg3"> &#8226;  TRAIN </span><span class="msg4">  &#8226;  DEPLOY </span><span class="msg5">  &#8226;  GOVERN</span>
            </div>
          </td>
        </tr>
      </tbody>
      </table>
      <footer>
        <p>Developed by Deloitte Portugal</p>
      </footer>
      </div>
    );
  }

  usernameChange = (e) =>{ 
    this.state.username = e.target.value;
  }
  
  passwordChange = (e) =>{ 
    this.state.password = e.target.value;
  }

  handleClick(){
    console.log('STATE: ', this.state);
    try {
      const hostname = window.location.hostname;
      axios.post("https://" + hostname + "/apikflogin", {"req-num": "1"}, {
        auth: {
          username: this.state.username,
          password: this.state.password
        },
        headers: {'x-from-login': 'true'}
      }).then(response => {
        console.log(response);
        // Login successful, get http.StatusSeeOther
        // redirect to central dashboard
        if (response.status === 205) {
          console.log("Login successfull");
          window.location = "/"
        } else {
          console.log("Unrecgonized response");
          this.setState({
            dialogTitle: 'Unrecgonized response',
          });
        }
      }).catch(e => {
        console.log('Exception: ', e);
        this.setState({
          dialogTitle: 'Username or password incorrect',
        });
      });
    } catch (e) {
      console.log('Exception: ', e);
    }
  }
}

export default Login;
