## Basic Auth Login Page

UI to login into Kubeflow cluster using basic auth.

#### This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


### Development environment

`docker-compose up`: Builds dev docker image locally for development

### Build production image

`make build`: Builds docker image containing the app for production.
